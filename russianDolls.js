/**
You are given a 2D array of integers envelopes where envelopes[i] = [wi, hi] represents the width and the height of an envelope.
One envelope can fit into another if and only if both the width and height of one envelope are greater than the other envelope's width and height.
Return the maximum number of envelopes you can Russian doll (i.e., put one inside the other).

Note: You cannot rotate an envelope.
 
Example 1:
Input: envelopes = [[5,4],[6,4],[6,7],[2,3]]
Output: 3
Explanation: The maximum number of envelopes you can Russian doll is 3 ([2,3] => [5,4] => [6,7]).

Example 2:
Input: envelopes = [[1,1],[1,1],[1,1]]
Output: 1
 
Constraints:
1 <= envelopes.length <= 105
envelopes[i].length == 2
1 <= wi, hi <= 105
*/

/** WIP : not yet found a best solution */
const maxEnvelopes = function (envelopesList) {
  const envelopes = [...envelopesList];

  if (envelopes.length === 0) {
    return 0;
  }

  // Sort the envelopes based on width
  const sortedEnvelopes = envelopes.sort((a, b) => {
    if (a[0] === b[0]) {
      return b[1] - a[1];
    }
    return a[0] - b[0];
  });

  const n = envelopes.length;
  const dp = new Array(n).fill(1);

  let maxCount = 1;

  for (let i = 1; i < n; i++) {
    for (let j = 0; j < i; j++) {
      if (sortedEnvelopes[i][1] > sortedEnvelopes[j][1]) {
        dp[i] = Math.max(dp[i], dp[j] + 1);
      }
    }
    maxCount = Math.max(maxCount, dp[i]);
  }

  return maxCount;
};

const russianDoll1 = [
  [5, 4],
  [6, 4],
  [6, 7],
  [2, 3],
];
const russianDoll2 = [
  [1, 1],
  [1, 1],
  [1, 1],
];

console.log("russianDoll1 =>", maxEnvelopes(russianDoll1));
console.log("russianDoll2 =>", maxEnvelopes(russianDoll2));
